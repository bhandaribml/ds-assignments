'''
Data Science Batch 28th June 2021 5PM-7PM
Assignment 1
Question: 21
Assignment of Bimal Bhandari
Coding done using PyCharm IDE
'''

#importing modules
import pandas as pd
import matplotlib.pyplot as plot
import statsmodels.api as sm
from seaborn import distplot
import warnings
warnings.filterwarnings("ignore")

Q21 = "Check whether the data follows normal distribution"
print(Q21)

q21a_data = pd.read_csv("Cars.csv")
q21b_data = pd.read_csv("wc-at.csv")

def draw_graphs(data):
    fig1 = sm.qqplot(data, line='q')
    fig1 = plot.title("QQ Plot for {0}".format(data.name))
    plot.show()
    fig2 = distplot(data, hist=True)
    fig2 = plot.title("Density and Histogram for {0}".format(data.name))
    plot.show()


Q21a = "Check whether the MPG of Cars follows Normal Distribution (Dataset: Cars.csv)"
print(Q21a)
print("Answer to Q21 a:")
draw_graphs(q21a_data["MPG"])

print("Based on the qq-plot and histogram, the MPG data is in normal distribution")
print()
print()
Q21b = "Check Whether the Adipose Tissue (AT) and Waist Circumference(Waist)  from wc-at data set  follows Normal Distribution (Dataset: wc-at.csv)"
print(Q21b)
print("Answer to Q21 b:")

draw_graphs(q21b_data["AT"])
print("Based on the qq-plot and histogram, the MPG data is not in normal distribution")
draw_graphs(q21b_data["Waist"])
print("Based on the qq-plot and histogram, the MPG data is not in normal distribution")