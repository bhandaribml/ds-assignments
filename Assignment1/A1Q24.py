'''
Data Science Batch 28th June 2021 5PM-7PM
Assignment 1
Question: 24
Assignment of Bimal Bhandari
Coding done using PyCharm IDE
'''

#importing modules
import math
from scipy import stats
import warnings
warnings.filterwarnings("ignore")

Q24 = "A Government  company claims that an average light bulb lasts 270 days. A researcher randomly selects 18 bulbs for testing. The sampled bulbs last an average of 260 days, with a standard deviation of 90 days. If the CEO's claim were true, what is the probability that 18 randomly selected bulbs would have an average life of no more than 260 days"
print(Q24)
print()

co_mean = 270
samp = 18
sp_mean = 260
sp_sdev = 90

df = samp - 1
t_value = (sp_mean - co_mean)/(sp_sdev/(math.sqrt(samp)))

print("Answer for Q24:")
print("Population Mean is: {0}, Sample Mean is: {1}, Sample Standard Deviation is: {2}, and Sample Size is: {3}".
      format(co_mean, sp_mean, sp_sdev, samp))
print("Calculating t-value....")
print("t-value is: {0}".format(t_value))
print()
print("Calculating probability...")
probability = stats.t.cdf(t_value, df)
print("Probability is {0} or {1}%".format(round(probability, 5), round(probability, 5)*100))
