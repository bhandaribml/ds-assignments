'''
Data Science Batch 28th June 2021 5PM-7PM
Assignment 1
Question: 7
Assignment of Bimal Bhandari
Coding done using PyCharm IDE
'''

#importing modules
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.api as sm
import warnings
warnings.filterwarnings("ignore")

Q7 = "Calculate Mean, Median, Mode, Variance, Standard Deviation, Range & comment about the values / draw inferences, for the given dataset. For Points,Score,Weigh, find Mean, Median, Mode, Variance, Standard Deviation, and Range and also Comment about the values/ Draw some inferences. (Use Q7.csv file)"
print(Q7)
print("Answer for Q7:")

q7_data = pd.read_csv('Q7.csv')

#q7_data.describe() function can be used to look mean, median, variance, and standard deviation

def calculations_for_column(column):
    column_mean = column.mean()
    column_median = column.median()
    column_mode = column.mode()
    # putting mode values in list as there might be multiple mode values
    formatted_column_mode = [val for val in column_mode]
    column_variance = column.var()
    column_stddev = column.std()
    column_range = column.max() - column.min()


    # printing outputs
    print("For {0}, the Mean is: {1}, Median is: {2}, Mode is: {3}, Variance is: {4}, Standard Deviation is: {5}, and Range is: {6}".format(column.name, round(column_mean, 4), round(column_median, 4), formatted_column_mode, round(column_variance, 4), round(column_stddev, 4), round(column_range, 4)))

    # plotting the qq plot
    # line = 'q' -- Options for the reference line to which the data is compared: i.e., A line is fit through the quartiles.
    fig = sm.qqplot(column, line='q')
    plt.title(column.name)
    plt.show()


calculations_for_column(q7_data["Points"])
calculations_for_column(q7_data["Score"])
calculations_for_column(q7_data["Weigh"])
