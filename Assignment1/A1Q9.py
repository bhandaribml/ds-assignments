'''
Data Science Batch 28th June 2021 5PM-7PM
Assignment 1
Question: 9
Assignment of Bimal Bhandari
Coding done using PyCharm IDE
'''

#importing modules
import pandas as pd
import matplotlib.pyplot as plot
from scipy.stats import kurtosis, skew
import warnings
warnings.filterwarnings("ignore")

Q9 = "Calculate Skewness, Kurtosis & draw inferences on the following data: \n1. Cars speed and distance (Use Q9_a.csv file)\n2. SP and Weight(WT) (Use Q9_b.csv file)"


q9a_data = pd.read_csv("Q9_a.csv")
q9b_data = pd.read_csv("Q9_b.csv")
print(q9a_data.head(2))
print(q9b_data.head(2))

#Calculating skewness and kurtosis
def calculate_skew_and_kurtosis(column):
    column_skew = skew(column)
    column_kurtosis = kurtosis(column)
    print("The skewness is: {0}, and kurtosis is: {1}, for {2}".format(round(column_skew, 5), round(column_kurtosis, 5), column.name))

    # print plots for skewness and kurtosis
    fig = column.plot(kind='density')
    plot.title(column.name)
    plot.show()


print("Answer for Q9 a:")
calculate_skew_and_kurtosis(q9a_data["speed"])
calculate_skew_and_kurtosis(q9a_data["dist"])
print()
print()
print("Answer for Q9 b:")
calculate_skew_and_kurtosis(q9b_data["SP"])
calculate_skew_and_kurtosis(q9b_data["WT"])
