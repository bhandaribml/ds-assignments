'''
Data Science Batch 28th June 2021 5PM-7PM
Assignment: Simple Linear Regression

Assignment of Bimal Bhandari
Coding done using PyCharm IDE
'''

import pandas as pd
from seaborn import distplot, regplot
import matplotlib.pyplot as plt
import statsmodels.formula.api as api

import warnings
warnings.filterwarnings("ignore")



def prediction_function(data_passed, col1_name, col2_name):
    print(data_passed.info())
    print("Printing correlation...")
    print(data_passed.corr())

    print()
    print("Drawing distplot for data...")
    fig, (fig1, fig2) = plt.subplots(ncols=2, sharey=True)
    distplot(data_passed[col1_name], hist=True, ax=fig1)
    fig1.set_title("DistPlot for {0}".format(col1_name))
    distplot(data_passed[col2_name], hist=True, ax=fig2)
    fig2.set_title("DistPlot for {0}".format(col2_name))
    plt.show()

    print()
    print()
    print("Fitting a linear regression model...")
    print("Drawing OLS plot...")
    col_a = data_passed[col1_name]
    col_b = data_passed[col2_name]
    model = api.ols('col_a~col_b', data=data_passed).fit()
    data_rp = regplot(x=col1_name, y=col2_name, data=data_passed)
    plt.show()
    print()
    print()
    print("Printing summary of the model...")
    print(model.summary())

    # now predicting the new values
    new_data = data_passed[col2_name]
    new_data.to_frame()
    new_data.name = "NewData"
    #print(new_data)
    data_predt = pd.DataFrame(new_data, columns=[col_a])
    predicted_data = model.predict({"NewData":data_predt})
    new_predicted_set = data_passed
    new_predicted_set["NewPredictedValue"] = round(predicted_data, 2)
    print('\n\n')
    print("Printing the table with new predicted values...")
    print(new_predicted_set)



question = "Build a simple linear regression model by performing EDA and do necessary transformations and select the best model using R or Python."
question1 = "Delivery_time -> Predict delivery time using sorting time"
print(question)
print(question1)
print("Answer to question 1:")

# reading csv file
deliveryTime = pd.read_csv("delivery_time.csv")
print(deliveryTime.info())
# renaming colunms names as they contains spaces
deliveryTime = deliveryTime.rename(columns={"Delivery Time":"DeliveryTime", "Sorting Time":"SortingTime"})
prediction_function(deliveryTime, 'DeliveryTime', 'SortingTime')

print('\n\n\n')
question2 = "Salary_hike -> Build a prediction model for Salary_hike"
print(question2)
salaryData = pd.read_csv("Salary_Data.csv")
print(salaryData.info())
# no need to rename column since column names doesn't have spaces
prediction_function(salaryData, 'Salary', 'YearsExperience')
